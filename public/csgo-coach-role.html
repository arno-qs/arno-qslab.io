<!DOCTYPE html>
<html lang="en-us">
<head>
<title>Thoughts on the role of a coach in Counter-Strike: Global Offensive</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="main.css">
<style>
table.coach-interaction-signals th, table.coach-interaction-signals td {
    padding: 0.5em;
}
</style>
</head>
<body>

<div id="header">

<div class="h1-container"><h1>Thoughts on the role of a coach in Counter-Strike: Global Offensive</h1><div class="horizontal-rule-gradient"></div></div>

<div class="table-of-contents">
<ol>
  <li><a href="#the-csgo-coach-rule-today">The CSGO coach role, today</a></li>
  <li><a href="#the-dota2-coach-rule-today">The DOTA 2 coach role, today</a></li>
  <li><a href="#enforcing-the-current-rules-at-lan">Enforcing the current rules at LAN</a></li>
  <li><a href="#my-vision-of-the-noble-goal">My vision of "the noble goal"</a></li>
  <li><a href="#the-implied-coaching-sea-change">The implied coaching sea change</a></li>

</ol>
</div>

</div> <!-- div#header -->

<div id="body">

<p>Before we start, let's acknowledge the fact that you (the reader) may not
consider me to be an expert at Counter-Strike.  I've barely played the game
(about ten matches lifetime) and I'm certainly not qualified to be a
professional player, coach, analyst, or caster/commentator.  That said, I
<em>am</em> an avid fan of the game, and I've easily watched hundreds of hours
of pro-level gameplay (about half of
<a href="https://eventvods.com/csgo">Eventvods' CSGO VOD library</a>, just for
starters).  That doesn't mean you should agree with me (and I didn't write this
to convince anybody of any particular point, anyway), but it does break my
opinions out of the "oh look, someone watched two games and thinks they know
everything" tier.  That's all I'm sayin'. :)</p>

<p>When I say "the role of the coach" here, I'm not talking about a coach's
overall role in the team organization.  This essay is specifically about the
<em>in-game</em> role, as far as how/if the coach is allowed to interact with
the players:</p>

<ol>
  <li>...physically (high-fives, pats on the back, chair shaking, etc.),
      and</li>
  <li>...visually (body movements in the players' field of view), and</li>
  <li>...aurally, including
    <ol>
      <li>...making noises that are loud enough to be audible to the players,
          and</li>
      <li>...speaking (yelling?) words that are loud enough to be audible to
          the players, and</li>
      <li>...most directly, being on the team's in-game voice comms.</li>
    </ol>
</ol>

<h2 id="the-csgo-coach-rule-today">The CSGO coach role, today</h2>

<p>Recently (by which I mean over the past few years), the world of
professional Counter-Strike has been forced to come to terms with the fact that
cheating is a very real, very concrete thing...even at LAN events, the
ostensible "sacred ground" of high-level CSGO.  From individual players'
getting physically pulled off their LAN rigs to the spectator cam coaching bug
scandal there's no denying it: cheating is in CSGO and it's not just one or two
bad eggs.</p>

<p>My point in saying this isn't to call anybody out, per se.  What I'm trying
to demonstrate here is that CSGO's coaching rules aren't some kind of
meanie-head edicts handed out because Valve gets its jollies from making the
lives of the people who play the game worse; they're a direct response to a
real situation in the real world where the community has, unfortunately,
confirmed that it absolutely can and will cheat if given the chance.</p>

<p>Currently (unless things have changed since I last checked, which is
possible), CSGO coaches are allowed to sit behind their players on the LAN
stage as long as they don't do anything that could be a signal to the players
(mostly this means staying quiet, but I suppose large enough physical movements
would also be a no-no).  Generally, I imagine the idea is that this has two
advantages for the team:</p>

<ol>
  <li>For the coach specifically, they still at least get to have <em>some</em>
      idea of how the players are feeling, since they can observe them from
      beind (hear them talk on comms, watch body language, etc.).</li>
  <li>When there's a time out, halftime, etc. the coach is right there to jump
      in when they <em>are</em> allowed to interact.</li>
</ol>

<p>A lot of people in the community are against these new coach interaction
rules; they want the coaches to be able to "be human" with their teams, and
argue that it's hard on emotionally-invested coaches to have to just sit and
watch while their players are going through a tough time, or not be able to
cheer when the team does something spectacular.  For my part, I agree; these
are sucky situations that suck, for exactly the reasons given.</p>

<p>
<a href="https://twitter.com/zonic/status/1454136181374001159">This Tweet from Zonic</a>
(<a href="https://nitter.net/zonic/status/1454136181374001159">nitter</a>)
(who was the coach of Astralis' CSGO team at the time) contains this
information from the tournament organizers of the 2021 PGL Major in
Stockholm:</p>

<blockquote>
Valve has instructed us to have stricter rules for coaches.  NO touching the
players except for timeout.  NO shouting at all.  Coaches can not even say
"nice".  They have to be quiet or we will have to tell coaches to leave the
tournament area.
</blockquote>

<p>Zonic's Tweet, and the bulk of the replies to it, pretty much cover the
indignant community reaction to a rule change that looks, on the surface, to be
mean-spirited and arbitrary at worst and nonsensical at best.</p>

<p>HOWEVER.</p>

<p>The simple fact of the matter is that if you want to eliminate coach
cheating, you have to eliminate everything that could be a signal.  For
example, consider the following yells from a coach at the end of a round:</p>

<table class="coach-interaction-signals">
<thead>
  <tr>
    <th>What the coach says</th>
    <th>What the real message is</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td>Yeah!</td>
    <td>Rush B next time</td>
  </tr><tr>
    <td>Yeah baby!</td>
    <td>Rush A next time</td>
  </tr><tr>
    <td>Wooooo!</td>
    <td>Eco round next time</td>
  </tr>
</tbody>
</table>

<p>Now, do I think this type of "coded messaging" has been done?  Not
specifically, no.  What about coded messages from chair shakes, shoulder
squeezes, or back slaps?  No, not that either, specifically.  I'm just saying
that if you're going to acknowledge that coach cheating is a thing that has
happened before, and could happen again, that is one way it could happen.</p>

<h2 id="the-dota2-coach-rule-today">The DOTA 2 coach role, today</h2>

<p>I know we're talking about CSGO here, but just to give a "contrasting
example", let's take a quick look at another esport that Valve runs tournaments
for (and they actually run The International directly, as opposed to simply
"overseeing" the CSGO majors that are run by other organizations like Blast,
ESL, Faceit, PGL, etc.).</p>

<p>The coaching interaction rule at The International is simple: once the game
proper starts, there is none.  Here's the relevant section of the ESL
tournament rules:</p>

<blockquote>
The coach is only allowed to communicate with the players until the end of the
drafting phase of the match. After the drafting phase is concluded, the coach
is not allowed to further communicate or interact with the players in any way
until after the match has concluded. Furthermore, the coach is not allowed to
be present around the players after the drafting phase has concluded, until the
end of the match.
</blockquote>

<p>The "drafting phase", for those unfamiliar with DOTA 2, is a character
pick/ban phase that's loosely equivalent to CSGO's map pick/ban phase (although
I'd say it's definitely more significant in DOTA 2).  There's probably
<em>some</em> benefit from having the coach's expertise during that phase, but
it's not a "quick reactions, real time" part of the game as esports generally
understands things; having an extra person there who can "focus on the
information without having to have hands on keyboard and mouse" isn't going to
help during the draft.</p>

<p>Then, once the draft is over, the coach has to leave the entire player area;
this is much stricter than even the new CSGO rules, where the coaches are
allowed to be in the player area, they just have to keep quiet so as to not
potentially signal the players.  In DOTA 2, the coaches <em>physically
can't</em> signal the players because they're not allowed to be anywhere near
them until the match is over.</p>

<p>Who could ever possibly know WTF Valve is ever thinking for anything, but
it's possible that they're trying to let CSGO be as "human" as possible while
still taking every reasonable precaution against cheating (but not going as far
as they do with DOTA 2).</p>

<h2 id="enforcing-the-current-rules-at-lan">Enforcing the current rules at LAN</h2>

<p>If you really wanted to bring the hammer down, while still keeping the
actual rules the same, I suppose you could do the following:</p>

<ol>
  <li>Put the coaches in their own, separate rooms (not in the same separate
      room, of course) with a referee present.</li>
  <li>The coach has access to the game in spectator mode and can watch from any
      of their players' cams, but cannot be in voice chat or type in the text
      chat.</li>
  <li>If the coach wants to call a time out, they tell the referee; the referee
      is then responsible for calling the time out and enabling the coach's
      voice comms for the duration of the time out.</li>
  <li>The coach is also allowed to type in text chat during the time out.</li>
</ol>

<p>Basically, this would bring CSGO in parallel with DOTA 2's coach interaction
rules, while still allowing for voice interaction during timeouts.  Potentially
you could also say they can go out and be with the players if they want, but
during a 30-second timeout I'm not sure how feasible that would really be.
It's an option, though, and maybe worth having if all you want to do as a coach
is go out and deliver some hype to your players.</p>

<h2 id="my-vision-of-the-noble-goal">My vision of "the noble goal"</h2>

<p>This is where I wax poetic on what I think might work for the best in the
long term, even if it means upheaval in the short term.  When it comes to
"professional esport tournament rules", I try to stick to this guiding
principle:</p>

<blockquote>
There should be as much similarity between casual, competitive, and
professional CSGO as possible.
</blockquote>

<p>Or, to put it more colloquially: "As it is in the PUGs, so shall it be on
LAN".</p>

<p>Now, Valve themselves have already kicked that squarely in the teeth with
their ridiculous "casual mode", but...there can still be hope for
"professional" versus "garden-variety competitive" games.  To be concise, let's
just lump everything that's not a professional offline LAN event into a single
"online" bucket.</p>

<p>For me, what this means is that we start from scratch and ask "What exists
in reasonable online play, and how can we mesh that with what we want for LAN
play?"  Obviously that's a <em>huge</em> question overall, but right now we're
just talking about coaching.</p>

<p>The first argument might be that well over 99% of online games are played
without any coach at all; does that mean we get rid of coaches for professional
play entirely?  I mean, <em>technically</em> that's a valid equivalency, but in
practical terms I'm pretty sure that's not what the community wants (it's
certainly not what <em>I</em> would prefer).  So we're keeping the
coaches...whew!</p>

<p>The easiest way to make a rule enforceable is to not make it in the first
place.  It's not practical to have any restrictions against coaching for online
play because you simply have no way of knowing what third-party voice chat
(Mumble, Discord, etc.) the players and their hypothetical coach are using.
So, in practical terms (and I'm guessing in the rules for a lot of actual
tournaments as well) there simply aren't any coach interaction rules when it
comes to online play.</p>

<p>Based on my guiding principle of "LAN play should mirror online play as much
as possible," to me that means that LAN play should also have unrestricted
coach interaction.  BOOM, there's the bombshell!</p>

<h2 id="the-implied-coaching-sea-change">The implied coaching sea change</h2>

<p>I honestly believe that unrestricted coach interaction could be good for the
game, but there's no denying it would usher in <em>huge</em> changes to the
meta.  The IGL role would evaporate overnight; any serious, competitive CSGO
team would become, essentially, a six-person squad (certainly all the
professional teams, who already <em>have</em> coaches).</p>

<p>In the interests of intellectual honesty, I will now present the reasons I
can imagine someone else would have for saying this is a bad idea:</p>

<ol>
  <li>CSGO is perfect the way it is and any change is, by definition, a change
      for the worse.  (It may sound like I'm being patronizing here, but I
      assure you I've encountered this exact argument much more than
      once.)</li>
  <li>At its core, CSGO is an action game; allowing a player to actively
      participate in the game without having to manage mechanical execution
      goes against that core essence.</li>
  <li>Having a player acting as a dedicated strategist/tactician would
      disincentivize the other players from participating in that part of the
      game, turning them into simple "all aim, no brain" vessels of the coach's
      will with no game sense, and no need for it.</li>
  <li>Having the coach play "dedicated IGL" would rob players who actually can
      shine in the current IGL role as both tacticians <em>and</em> players of
      the chance to show how truly legendary they are.</li>
</ol>

<p>I actually think that "all aim, no brain" point may have a <em>tiny</em> bit
of traction, but <em>nowhere near</em> as much as a lot of people claim.
Remember, four of the five teammates are already taking their orders from an
IGL, and they're not "mind controlled".  The only reason I think it might be
<em>slightly</em> different is that perhaps some teams would be more likely to
implicitly trust an IGL who wasn't playing, since their full attention is on
the minimap and team tactics.  But again, the game already has legendary IGLs
who will frag your face off while simultaneously living rent-free in the other
team's heads.</p>

<p>As for the "god-tier current IGLs will be less legendary" point...I do have
to concede that.  I suppose a hypothetical IGL could be <em>so</em> god-tier
that even an in-game coach can't out-IGL them, but I can't in good conscience
not concede that there's a cross-section of current IGLs who would just become
"excellent players" afterward (and not eventually rise even higher as
players).</p>

<p>So that's the "here's why I don't agree, or at least don't agree
<em>enough</em>, with your counter-arguments" section.  Now, on to my actual
arguments <em>for</em> the change:</p>

<ol>
  <li>It solves the current coach interaction rules problem instantly and
      completely.  Everybody gets an in-game coach, so it's 100% fair, and
      there's no concern about "sneaky comms" 'cause they're right there in
      voice chat with the rest of the team at all times.</li>
  <li>Rather than lamenting the loss of the "god-tier IGL", I instead imagine a
      game where:
    <ol>
      <li>...the coach's ability to communicate in real time based on real-time
          game information leads to an even higher, more mind-boggling level of
          strategic and tactical meta because the coach doesn't have to juggle
          attention between information and angle-holding, and</li>
      <li>...the other players can call out info on comms knowing that the
          person listening for it will be razor-sharp tuned in for just that,
          and not tunnel-visioned on an AWP angle or lining up a smoke.</li>
    </ol>
</ol>

<p>To put it more generally, I think that having a sixth player ('cause that's
what it'd really be at that point) acting as a dedicated IGL could raise the
strategic and tactical standard of CSGO <em>even higher</em> than it's already
gone.  Would the game be <em>different</em>?  Yes, of course...but I'm not
opposed to evolving the game if it's for the better.</p>

</div> <!-- div#body -->

<div id="footer">
<div class="horizontal-rule"></div>
<p>
<a href="http://jigsaw.w3.org/css-validator/validator?uri=https://arno-qs.gitlab.io/csgo-coach-role">Valid CSS3</a> -
<a href="https://validator.w3.org/nu/?doc=https://arno-qs.gitlab.io/csgo-coach-role">Valid HTML5</a>
</p>
</div> <!-- div#footer -->

</body>
</html>
