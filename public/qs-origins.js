/****************************************************************************/
/* Choose a string from the list of 100%-accurate origins for the "QS" tag, */
/* and update the quote box                                                 */
/****************************************************************************/

/*
Future ideas for other 100%-accurate origins:

Quick   Silver
Quite   Silly
Quarrel Sublimation
Quack   Shack
Quash   Stupidity
Quiz    Show
*/

qs_choices = [

`So this one time, some friends and I threw a house party and (as as these things do) it led to a lot of drunken debauchery.  Toward the end of the night, we were hungry and wanted to go eat at a local diner, but nobody was in any condition to drive.  Out of nowhere, two other friends came downstairs; we hadn't seen them for hours and it turned out they'd just been upstairs talking ("talking" :P) the whole time.  We told them of our plight; one of them had to leave right away, but the other one thought about it for a minute and after extensive peer pressure and puppy-dog looks, she exclaimed, "Fine, yes!  I am <strong>Queen Sober</strong> and I will take you to the Land of Jack, in search of his tasty Box."  As a show of thanks, we all added QS to our IRC nicknames and called her "Your Highness" the next day.`,

`Sometimes you feel out of sorts with the world; like you don't fit in and don't have your own place.  That's not to say the feeling lasts forever, but in that moment you're an island unto yourself; what's important to you isn't important to other people, or not in the right amounts.  You're liked, but not valued or needed.  Sometimes, when that special kind of loneliness washes over you, the best answer to the world is <strong>quiet subtraction</strong>.`,

`One thing I've always prided myself on is the ability to identify the true cause of a problem, its effects, and optimal resolution.  It sounds obvious, but you'd be surprised how many people get bogged down in "politics", or "emotional responses".  Now, I'm not saying that it's good to be politically tone-deaf, or that emotional responses aren't reasonable and valid; those are both very important personal and social dynamics that shouldn't be apologized for or ignored.  But the ability (or perhaps it's just the <em>will</em>) to see them for what they are, and not let them cause you to lose sight of the cause and effects's true nature, isn't as common as you might think.  Ultimately you need to move from one to the other.  Cause, effect.  <strong>Query, solution</strong>.`,

`I <em>love</em> first-person shooter games; been playing them for as long as I can remember.  Got pretty good for a while, too, although it's been a very long time since I did any kind of serious competitive multiplayer gaming and my skills have atrophied along with my desire to put up with most online games' community members.  Still, even if it <em>was</em> a long time ago, when you can consistently hit a three-millimeter-diameter target in a matter of milliseconds, they call you "Mr. <strong>Quick Shot</strong>".`,

]

document.getElementById("qs-origins").innerHTML = qs_choices[Math.floor(Math.random() * 1000) % qs_choices.length]

// EOF
////////
