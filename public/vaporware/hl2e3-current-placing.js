// First of all, apologies to anybody who's looking at this expecting to see
// something interesting; I simply hardcoded a bunch of stuff in here 'cause I
// realized at the last minute I wanted to expand the scope, but I don't want
// to do a bunch of work generalizing everything a) right now, and/or b)
// perhaps ever if nobody (including myself) ever gets interested enough to
// expand the scope beyond the initial joke.

function update_hl2e3_placing() {
    var dnf_vaporware_duration = 400370401
    var placing = ""
    if ( Number(document.getElementById("hl2e3-current-duration").innerText) > dnf_vaporware_duration ) {
        placing = "1"
    } else {
        placing = "2"
    }

    document.getElementById("hl2e3-placing-1").innerText = placing
    document.getElementById("hl2e3-placing-2").innerText = placing
}

// This syntax causes the code to run only when the DOM has finished loading
window.addEventListener('load', hl2e3_current_placing_main);

function hl2e3_current_placing_main() {
    update_hl2e3_placing()
    window.setInterval(update_hl2e3_placing,  1000)
};

// EOF
// /////
