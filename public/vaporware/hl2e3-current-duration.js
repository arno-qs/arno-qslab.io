// First of all, apologies to anybody who's looking at this expecting to see
// something interesting; I simply hardcoded a bunch of stuff in here 'cause I
// realized at the last minute I wanted to expand the scope, but I don't want
// to do a bunch of work generalizing everything a) right now, and/or b)
// perhaps ever if nobody (including myself) ever gets interested enough to
// expand the scope beyond the initial joke.
//
// 1198670399 is the epoch timestamp of HL2:E3's release estimate, by the way.

function update_hl2e3_duration() {
  document.getElementById("hl2e3-current-duration").innerText = Math.floor(Date.now() / 1000) - 1198670399
}

// This syntax causes the code to run only when the DOM has finished loading
window.addEventListener('load', hl2e3_current_duration_main);

function hl2e3_current_duration_main() {
    update_hl2e3_duration()
    window.setInterval(update_hl2e3_duration, 1000)
};

// EOF
// /////
