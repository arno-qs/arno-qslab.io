// Shout-outs to all the NoScript users (like me!)
//
// This Javascript will update the parts of the page that can benefit from
// scripting but are initially filled with static HTML in case the user agent
// has scripting disabled.

// ////////////////////////////////////////////////////////////////////////////

function replace_timestamp_with_timer() {
    // <div id="timer-container">
    //     <div id="hl3-vaporware-timer"></div>
    //     <div id="timer-explanation">(until 10:00:01 UTC on Wednesday, September 2, 2020)</div>
    // </div>
    //
    // <div id="timer-container">
    //     <div id="rollover-time">Wednesday, September 2, 2020</div>
    //     <div id="rollover-time">10:00:01 UTC</div>
    // </div>
    var timer_container = document.getElementById("timer-container")

    document.getElementById("rollover-date").remove()
    document.getElementById("rollover-time").remove()

    var timer_div = document.createElement("div")
    timer_div.setAttribute("id", "hl3-vaporware-timer")

    var timer_explanation_div = document.createElement("div")
    timer_explanation_div.setAttribute("id", "timer-explanation")
    timer_explanation_div.innerText = "(until 10:00:01 UTC on Wednesday, September 2, 2020)"

    timer_container.appendChild(timer_div)
    timer_container.appendChild(timer_explanation_div)
}

function add_table_columns() {
}

// ////////////////////////////////////////////////////////////////////////////

// This syntax causes the code to run only when the DOM has finished loading
window.addEventListener('load', graceful_upgrade);

function graceful_upgrade() {
    replace_timestamp_with_timer()
};

// EOF
// /////
