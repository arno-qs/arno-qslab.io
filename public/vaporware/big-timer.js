/*
  Half-Life 3 vaporware countdown timer

  Many thanks to these pages, for providing the code I modified:
    http://forum.codecall.net/topic/51639-how-to-create-a-countdown-timer-in-javascript
    http://css-tricks.com/snippets/jquery/run-javascript-only-after-entire-page-has-loaded
*/

function replace_timestamp_with_timer() {
    // <div id="timer-container">
    //     <div id="hl3-vaporware-timer"></div>
    //     <div id="timer-explanation">(until 10:00:01 UTC on Wednesday, September 2, 2020)</div>
    // </div>
    //
    // <div id="timer-container">
    //     <div id="rollover-time">Wednesday, September 2, 2020</div>
    //     <div id="rollover-time">10:00:01 UTC</div>
    // </div>
    var timer_container = document.getElementById("timer-container")

    document.getElementById("rollover-date").remove()
    document.getElementById("rollover-time").remove()

    var timer_div = document.createElement("div")
    timer_div.setAttribute("id", "hl3-vaporware-timer")

    var timer_explanation_div = document.createElement("div")
    timer_explanation_div.setAttribute("id", "timer-explanation")
    timer_explanation_div.innerText = "(until 10:00:01 UTC on Wednesday, September 2, 2020)"

    timer_container.appendChild(timer_div)
    timer_container.appendChild(timer_explanation_div)
}

function replace_flavor_text() {
    document.getElementById("js-or-not-description").innerText = "the amount of time left until that happens"
}

function add_table_columns(current_duration) {
    var current_duration_th = document.createElement("th")
    current_duration_th.innerText = "Current duration"

    var placing_1_th        = document.createElement("th")
    placing_1_th.innerText = "Placing (1)"

    var placing_2_th        = document.createElement("th")
    placing_2_th.innerText = "Placing (2)"

    var current_duration_td = document.createElement("td")
    current_duration_td.setAttribute("id", "hl2e3-current-duration")

    var placing_1_td        = document.createElement("td")
    placing_1_td.setAttribute("id", "hl2e3-placing-1")

    var placing_2_td        = document.createElement("td")
    placing_2_td.setAttribute("id", "hl2e3-placing-2")

    // <table id="contenders" class="time-table">
    // <thead>
    // <tr id="contender-table-header-row">
    //   <th>Name of game, etc.</th>
    //   <th>Initial release estimate</th>
    // </tr>
    // </thead>
    // <tbody>
    //
    // <tr id="hl2e3-info-row">
    //   <td><a href="items/half-life-2-episode-3.html">Half-Life 2: Episode 3</a></td>
    //   <td>2007-12-26 11:59:59 UTC</td>
    // </tr>

    var contender_table_header_row = document.getElementById("contender-table-header-row")
    contender_table_header_row.appendChild(current_duration_th)
    contender_table_header_row.appendChild(placing_1_th)
    contender_table_header_row.appendChild(placing_2_th)

    var hl2e3_info_row = document.getElementById("hl2e3-info-row")
    hl2e3_info_row.appendChild(current_duration_td)
    hl2e3_info_row.appendChild(placing_1_td)
    hl2e3_info_row.appendChild(placing_2_td)
}

function update_timer() {
    timer = document.getElementById("hl3-vaporware-timer");

    /* Collect the relevant UTC unixtime timestamps */
    var current_unixtime  = Math.floor(Date.now() / 1000);
    var hl3_vaporware_day = 1599040801; /* 2020-09-02 10:00:01 UTC */

    /* Calculate the difference */
    seconds = hl3_vaporware_day - current_unixtime;

    if (seconds <= 0) {
        timer.innerHTML = "Valve, you are go for release!";
        document.getElementById("timer-explanation").innerHTML = "(it's past 10:00:01 UTC on Wednesday, September 2, 2020)"
        return;
    } else {
        var years   = Math.floor(seconds / 31536000); seconds -= years   * 31536000;
        var weeks   = Math.floor(seconds / 604800);   seconds -= weeks   * 604800;
        var days    = Math.floor(seconds / 86400);    seconds -= days    * 86400;
        var hours   = Math.floor(seconds / 3600);     seconds -= hours   * 3600;
        var minutes = Math.floor(seconds / 60);       seconds -= minutes * 60;

        /* A little cosmetics dancing */
        var years_string = ((years > 1) ? "years" : "year");
        var weeks_string = ((weeks > 1) ? "weeks" : "week");
        var days_string  = ((days  > 1) ? "days"  : "day" );

        /* Build the textual time string to display on the page */
        var time_string = "";
        time_string  = ((years > 0) ? years + " " + years_string + ", " : "");
        time_string += ((weeks > 0) ? weeks + " " + weeks_string + ", " : "");
        time_string += ((days  > 0) ? days  + " " + days_string  + ", " : "");
        time_string += leading_zero(hours);
        time_string += ":";
        time_string += leading_zero(minutes);
        time_string += ":";
        time_string += leading_zero(seconds);

        timer.innerHTML = time_string
    }
}

function leading_zero(time) {
    return (time < 10) ? "0" + time : + time;
}

// ////////////////////////////////////////////////////////////////////////////

replace_flavor_text()
replace_timestamp_with_timer()
add_table_columns()
update_timer()

/* This syntax causes the code to run only when the DOM has finished loading */
window.addEventListener('load', timer_onload);

function timer_onload() {
    window.setInterval(update_timer, 1000);
}

/* EOF */
